import React, {Component} from 'react';
import './App.css';
import './card-css/cards.css';
import Card from "./Card";

class App extends (Component) {

  state = {
    cards: [
      {number: '7', suit: '♦'},
      {number: 'K', suit: '♣'},
      {number: 'Q', suit: '♥'},
      {number: 'A', suit: '♠'},
      {number: 7, suit: '♠'}
    ]
  };

  getRandomCard = () => {
    const rank = {
      two: 2,
      three : 3,
      four: 4,
      five: 5,
      six: 6,
      seven: 7,
      eight: 8,
      nine: 9,
      ten: 10
    }
    const suit = {
      spades: '♠',
      diams: '♦',
      hearts: '♥',
      clubs: '♣'
    }
    const cards = [];

    for (let key in rank) {
      for(let num in suit){
        const card = {rank: rank[key], suit: suit[num]}
        cards.push(card);
      }
    }

    const  rand = cards[Math.floor(Math.random() * cards.length)];
    console.log(rand);
  };

  // getSaveState = () => {
  //   const newCrads = [...this.state.cards];
  //
  // }

  render() {
    return (
        <div className="App">
          <button onClick={this.getRandomCard}>Click me</button>
          <div className="playingCards faceImages">
            <Card
                number= {this.state.cards[0].number}
                suit= {this.state.cards[0].suit}
                cardClass={this.state.cards[0].cardClass}/>
            <Card
                number= {this.state.cards[1].number}
                suit= {this.state.cards[1].suit}
                cardClass={this.state.cards[1].cardClass}/>
            <Card
                number= {this.state.cards[2].number}
                suit= {this.state.cards[2].suit}
                cardClass={this.state.cards[2].cardClass}/>
            <Card
                number= {this.state.cards[3].number}
                suit= {this.state.cards[3].suit}
                cardClass={this.state.cards[3].cardClass}/>
            <Card
                number= {this.state.cards[4].number}
                suit= {this.state.cards[4].suit}
                cardClass={this.state.cards[4].cardClass}/>
          </div>
        </div>
    );
  }
}

export default App;
