import React from 'react';

const Card = props => {
  return (
      <div className="card spades rank-${props.num}">
        <span className="rank">{props.number}</span>
        <span className="suit">{props.suit}</span>
      </div>
  )
}

export default Card;